var dgram = require('dgram');
var binary = require('binary');
var util = require('util');
var EventEmitter = require("events").EventEmitter;


var deffLocalPort = 54209;
var deffRemotePort = 9910;
var deffRemoteHost = "192.168.10.240";

var debug = false;

util.inherits(Atem, EventEmitter);

function Atem(options){
	this.localPort = deffLocalPort;
	this.remotePort = deffRemotePort;
	this.remoteHost = deffRemoteHost;
	this.sessionID = null;
	this.hasInitialized = false;
	this.localPacketIdCounter = 1;
	this.state = {};
	if(options){
		this.localPort = options.localPort ? options.localPort : deffLocalPort;
		this.remotePort = options.remotePort ? options.remotePort : deffRemotePort;
		this.remoteHost = options.remoteHost ? options.remoteHost : deffRemoteHost;
	}

	EventEmitter.call(this);
}

Atem.prototype.start = function(){
	var self = this;
	if(debug) log.info("starting atem client");
	this.client = dgram.createSocket('udp4');
	this.client.bind(this.localPort);

	this.client.on('listening', function(){
		var address = self.client.address();
	    if(debug) log.info('UDP server listening on ' + address.address + ":" + address.port);
		self.sendHelloMessage();    
	});

	this.client.on('message', function(message, remote){
		self.parseMessage(message, remote);
	});
}

Atem.prototype.stop = function(){
	if(this.client){ 
		this.client.close(); 
		this.client = null; 
		this.sessionID = null;
		this.hasInitialized = false;
		this.state = {};
	}
	if(debug) log.info("atem client stopped");
}

Atem.prototype.sendHelloMessage = function(){
	if(debug) log.info("sending hello message");
	//var msg = new Buffer([0x10, 0x14, 0x3F, 0xBB, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]);
	var msg = new Buffer([0x10, 0x14, 0x53, 0xAB, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3A, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]);
	this.send(msg);
}

Atem.prototype.sendConnectHelloAnswerString = function(){
	if(debug) log.info("sending connect hello answer message");
	var msg = new Buffer([0x80, 0x0c, 0x53, 0xab, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00]);
	this.send(msg);
}

Atem.prototype.send = function(buff, callback){
	this.client.send(buff, 0, buff.length, this.remotePort, this.remoteHost, callback);
}

Atem.prototype.parseMessage = function(message, remote){
	var data = new Buffer(message);
	if(!this.sessionID){
		this.sessionID = data[15] ? data[15] : 0;
		if(debug) log.info("Session id: 0x" + this.sessionID.toString(16));
		this.sendConnectHelloAnswerString();
		this.emit("connected");
		return;
	}
	this.parseData(data);
}

Atem.prototype.parseData = function(packet){
	if(debug) log.info("parsing incoming packet");
	if(debug) log.debug("packet length: " + packet.length);

	var packetHeader = packet.slice(0, 12);
	var packetSize = word(packetHeader[0] & b("00000111"), packetHeader[1]);
	//log.debug("packet size: " + packetSize);

	var lastRemotePacketID = new Buffer([packetHeader[10], packetHeader[11]]);

	// log.debug("first bypte : " + toBin(packetHeader[0].toString(2)));
	// log.debug("second bypte: " + toBin(packetHeader[1].toString(2)));

	var command = packetHeader[0];
	var commandACK = command & b("00001000") ? true : false; // If true, ATEM expects an acknowledgement answer back!
	var commandInit = command & b("00010000") ? true : false; // If true, ATEM expects an acknowledgement answer back!
	// The five bits in "command" (from LSB to MSB):
	// 1 = ACK, "Please respond to this packet" (using the _lastRemotePacketID). Exception: The initial 10-20 kbytes of Switcher status
	// 2 = ?. Set during initialization? (first hand-shake packets contains that)
	// 3 = "This is a retransmission". You will see this bit set if the ATEM switcher did not get a timely response to a packet.
	// 4 = ? ("hello packet" according to "ratte", forum at atemuser.com)
	// 5 = "This is a response on your request". So set this when answering...
	
	// if(commandACK) log.debug("command ACK: true");
	// else log.debug("command ACK: false");

	// if(commandInit) log.debug("command Init: true");
	// else log.debug("command Init: false");

	// If a packet is 12 bytes long it indicates that all the initial information 
	// has been delivered from the ATEM and we can begin to answer back on every request
	// Currently we don't know any other way to decide if an answer should be sent back...
	if((packetSize == 12) && (!this.hasInitialized)){
		this.hasInitialized = true;
	}

	if((packetSize > 12) && !commandInit){
		this.parsePacket(packet);
	}

	if(this.hasInitialized && commandACK){
		this.sendAnswerPacket(lastRemotePacketID);
	}
}

Atem.prototype.parsePacket = function(data){
	if(debug) log.info("parsing packet commands")
	var idx;	// General reusable index usable for keyers, mediaplayer etc below.
	// If packet is more than an ACK packet (= if its longer than 12 bytes header), lets parse it:
	var indexPointer = 12;
	while(indexPointer < data.length){
		var cmd = data.slice(indexPointer, indexPointer + 8);
		//log.trace("cmd: " + cmd);
		//log.debug("cmd length: " + cmd.length);
		var cmdLength = word(cmd[0], cmd[1]);
		//log.info("command length: " + cmdLength);
		var cmdStr = new String(cmd.slice(4, 8));
		if(debug) log.info("command: " + cmdStr);
		var cmdData = data.slice(indexPointer + 8, indexPointer + cmdLength);

		// If length of segment larger than 8 (should always be...!)
		if(cmdLength > 8){
			if(cmdStr == "AMLv"){

			} else

			// Extract the specific state information we like to know about:

			// Program Bus status
			// 0 - black
			// 1 - cam-1
			// 2 - cam-2
			// 3 - cam-3
			// 4 - cam-4
			// 5 - cam-5
			// 6 - cam-6
			// 1000 - bars
			// 2001 - color-1
			// 2002 - color-2
			// 3010 - media-1
			// 3020 - media-2
			if(cmdStr == "PrgI"){
				var PrgI;
				if(!this.version42()){
					PrgI = cmdData[1];
				}else{
					PrgI = (cmdData[2]<<8) | cmdData[3];
				}
				if(debug) log.info("PrgI: " + this.data.PrgI);
				this.setState("programBus", PrgI);
			} else

			// PreviewBus status
			// the same statuses as programm
			if(cmdStr == "PrvI"){
				var PrvI;
				if(!this.version42()){
					PrvI = cmdData[1];
				}else{
					PrvI = (cmdData[2]<<8) | cmdData[3];
				}
				this.setState("previewBus", PrvI);
			} else 

			// Firmware version
			if(cmdStr == "_ver"){
				firmware = {};
				// Firmware version, "left of decimal point" (what is that called anyway?)
				firmware.version = cmdData[1];
				// // Firmware version, decimals ("right of decimal point")
				firmware.subversion = cmdData[3];
				this.setState("firmware", firmware);
			} else

			// Tally status for inputs 1-8
			if(cmdStr == "TlIn"){
				var count = cmdData[1];
				if(count > 16) count = 16;

				var tally = []; 
				for(var i = 0; i < 16; i++) tally.push(cmdData[2+1]);

				this.setState("tally", tally);
			} else

			// Time???
			if(cmdStr == "Time"){
				
			} else

			// Transition Preview
			if(cmdStr == "TrPr"){
				
			} else

			// Transition Position
			if(cmdStr == "TrPs"){
				
			} else

			// Transition Style and Keyer on next transition
			if(cmdStr == "TrSS"){
				
			} else

			// Fade To Black State
			if(cmdStr == "FtbS"){
				
			} else

			// Fade To Black - Positions(?) (Transition Time in frames for FTB): 0x01-0xFA
			if(cmdStr == "FtbP"){
				
			} else

			// Mix Transition Position(?) (Transition Time in frames for Mix transitions.): 0x01-0xFA
			if(cmdStr == "TMxP"){
				
			} else

			// Downstream Keyer state. Also contains information about the frame count in case of "Auto"
			if(cmdStr == "DskS"){
				
			} else

			// Downstream Keyer Tie
			if(cmdStr == "DskP"){
				
			} else

			// Upstream Keyer on
			if(cmdStr == "KeOn"){
				
			} else

			// Color Generator Change
			if(cmdStr == "ColV"){
				
			} else

			// Media Player Clip Enable
			if(cmdStr == "MPCE"){
				
			} else

			// Aux Output Source
			if(cmdStr == "AuxS"){
				
			} else

			// Name
			if(cmdStr == "_pin"){
				
			} else

			// Audio Monitor Tally (on/off settings)
			if(cmdStr == "AMTl"){
				
			} else

			// Note for future reveng: For master control, volume at least comes back in "AMMO" (CAMM is the command code.)
			if(cmdStr == "AMIP"){
				
			} else

			// Audio Monitor Levels
			if(cmdStr == "AMLv"){
				
			} else

			// Video format (SD, HD, framerate etc.)
			if(cmdStr == "VidM"){
				
			} else {

			}


		}else{
			if(debug) log.error("cmd length less than 8 bytes");
		}

		indexPointer += cmdLength;
	}
}

Atem.prototype.setState = function(key, val){
	this.state[key] = val;
	this.emit("changed", key, val);
}

Atem.prototype.sendAnswerPacket = function(lastRemotePacketID){
	if(debug) log.info("sending answer packet");
	var buff = new Buffer(12);
	//all zeros
	for(var i = 0; i < buff.length; i++) buff[i] = 0;
  
	buff[2] = 0x80;  // ??? API
  	buff[3] = this.sessionID;  // Session ID
  	buff[4] = lastRemotePacketID[0];  // Remote Packet ID, MSB
  	buff[5] = lastRemotePacketID[1];  // Remote Packet ID, LSB
  	buff[9] = 0x41;  // ??? API
  	// The rest is zeros.

  	// Create header:
	var returnPacketLength = 10+2;
	buff[0] = returnPacketLength/256;
	buff[1] = returnPacketLength%256;
	buff[0] = buff[0] | b("1000 0000");

	this.send(buff);
}

Atem.prototype.sendCommand = function(cmd, data){
	if(typeOf(data) == "number") data = new Buffer([data]);
	if(typeOf(data) == "string") data = new Buffer(data);

	// 20 header bytes + data length
	var buff = new Buffer(20 + data.length);
	for(var i = 0; i < buff.length; i++) buff[i] = 0;

	buff[2] = 0x80;  // ??? API
	buff[3] = this.sessionID;  // Session ID
	buff[10] = this.localPacketIdCounter/256;  // Remote Packet ID, MSB
	buff[11] = this.localPacketIdCounter%256;  // Remote Packet ID, LSB

	// Command
	for(var i = 0; i < 4; i++) buff[12+4+i] = cmd.charCodeAt(i);

	// Command value (after command):
	for (var i = 0; i < data.length; i++) buff[12+4+4+i] = data[i];

	// Command length:
	buff[12] = (4+4+data.length)/256;
	buff[12+1] = (4+4+data.length)%256;

	// Create header:
	var returnPacketLength = 10+2+(4+4+data.length);
	buff[0] = returnPacketLength/256;
	buff[1] = returnPacketLength%256;
	buff[0] = buff[0] | b("00001000");


	log.debug(JSON.stringify(buff));

	this.localPacketIdCounter++;

	this.send(buff);
}

Atem.prototype.version42 = function(){
	// ATEM Control Panel software v. 4.2 = firmware version 2.12
	var v = this.state.firmware.version;
	var s = this.state.firmware.subversion;
	return (v>2) || (v>=2 && s>=12);
}

/*============ Functions ============*/

function toBin(str){
	while(str.length < 8) str = "0" + str;
	return "0b" + str;
}

function b(str){
	var str = str.replace(/\s+/g, "");
	var pos = 1;
	var num = 0;
	for(var i = str.length -1; i >= 0; i--){
		if(str[i] == "1") num += pos;
		pos *= 2;
	}
	return num;
}

function word(b1, b2){
	var buf = new Buffer([b1, b2]);
	return binary.parse(buf).word16bu("num").vars.num;
}

function typeOf(value) {
    var s = typeof value;
    if (s === 'object') {
        if (value) {
            if (Object.prototype.toString.call(value) == '[object Array]') s = 'array';
        } else {
            s = 'null';
        }
    }
    return s;
}


/*============ Exports ============*/

module.exports = Atem;



